import 'package:book_tracker/screens/main_screen_page.dart';
import 'package:book_tracker/widgets/input_decoration.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class CreateAccountForm extends StatelessWidget {
  const CreateAccountForm({
    Key? key,
    required GlobalKey<FormState> formKey,
    required TextEditingController emailTextController,
    required TextEditingController passwordTextController,
  }) : _formKey = formKey, _emailTextController = emailTextController, _passwordTextController = passwordTextController, super(key: key);

  final GlobalKey<FormState> _formKey;
  final TextEditingController _emailTextController;
  final TextEditingController _passwordTextController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Text(
            'Please enter a valid email and a password that is at least 6 characters long'
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextFormField(
              validator: (value) {
                return value!.isEmpty ? 'Please, enter email' : null;
              },
              controller: _emailTextController,
              decoration: buildInputDecoration(
                  label: 'Enter email',
                  hintText: 'john@me.com'
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextFormField(
              validator: (value) {
                return value!.isEmpty ? 'Please, enter password' : null;
              },
              controller: _passwordTextController,
              obscureText: true,
              decoration: buildInputDecoration(
                  label: 'Password',
                  hintText: ''
              ),
            ),
          ),
          SizedBox(height: 20,),
          TextButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                FirebaseAuth.instance
                  .createUserWithEmailAndPassword(
                    email: _emailTextController.text,
                    password: _passwordTextController.text
                  )
                  .then((value) {
                    FirebaseAuth.instance
                      .signInWithEmailAndPassword(
                        email: _emailTextController.text,
                        password: _passwordTextController.text
                      )
                      .then((value) {
                        return Navigator.push(context, MaterialPageRoute(builder: (context) => MainScreenPage()));
                      });
                  });
              }
            },
            child: Text('Create account'),
            style: TextButton.styleFrom(
                primary: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                backgroundColor: Colors.amber,
                textStyle: TextStyle(
                  fontSize: 15,
                )
            ),
          ),
        ],
      ),
    );
  }
}
